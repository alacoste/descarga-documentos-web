import urllib.request


class Robot:
    def __init__(self, url):
        self.url = url
        self.downloaded = False
        self.response = ''

    def retrieve(self):
        if self.downloaded == False:
            self.downloaded = True
            document = urllib.request.urlopen(self.url)
            self.response = document.read().decode('utf-8')
            print("Descargando url...")

    def show(self):
        self.retrieve()
        print(self.response)

    def content(self):
        return self.response


class Cache:
    def __init__(self):
        self.documents = {}

    def show_all(self):
        print(self.documents.keys())
        print("")

    def retrieve(self, url):
        if not url in self.documents.keys():
            self.documents[url] = Robot(url)
            self.documents[url].retrieve()

    def show(self, url):
        self.retrieve(url)
        self.documents[url].show()

    def content(self, url):
        self.retrieve(url)
        str = self.documents[url].content()
        return str


def displayMenu():
    print("1._Download url")
    print("2._Exit")


def getOption():
    valid = False
    option = 0
    while not valid:
        displayMenu()
        try:
            option = int(input("Choose one option: "))
            if 1 <= option <= 2:
                valid = True
            else:
                raise ValueError
        except ValueError:
            print("Incorrect option, try a number [1 - 2]")
    return option


def downloadUrl(caches):
    url = input("Write an Url to download: ")
    caches.retrieve(url)
    caches.show(url)
    print(caches.content(url))
    caches.show_all()
    print("")


if __name__ == '__main__':
    exit = False
    caches = Cache()
    while not exit:
        try:
            option = getOption()
            if option == 1:
                downloadUrl(caches)
            else:
                raise KeyboardInterrupt
        except KeyboardInterrupt:
            exit = True
            print("Finishing")
        except Exception:
            print("Invalid Url")